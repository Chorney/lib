const _ = require('lodash');
const { compile } = require('json-schema-to-typescript');
const fs = require('fs');

const replaceRefs = (obj, spec) => {
  const newObj = {};
  Object.keys(obj).forEach((key) => {
    if (key === '$ref') {
      const refObject = _.get(spec, obj[key].split('#/')[1].replace(/\//g, '.'));
      Object.assign(newObj, replaceRefs(refObject, spec));
      return;
    }

    // MANUALLY ADD BOTH ORIGINAL TYPE AND null, AS OPENAPI DOESN'T ALLOW MULTIPLE TYPES
    // BUT JSON SCHEMA DOES
    if (key === 'default' && ['null', null].includes(obj[key])) {
      newObj[key] = null;
      newObj.type = [obj.type, 'null'];
      return;
    }

    if (key === 'nullable' && obj[key]) {
      newObj.type = [obj.type, 'null'];
      return;
    }

    if (_.isArray(obj[key])) {
      newObj[key] = [];
      obj[key].forEach((item) => {
        if (item !== null && typeof item === 'object') {
          newObj[key].push(replaceRefs(item, spec));
        } else {
          newObj[key].push(item);
        }
      });
      return;
    }

    if (obj[key] !== null && typeof obj[key] === 'object') {
      newObj[key] = replaceRefs(obj[key], spec);
      return;
    }

    newObj[key] = obj[key];
  });
  return newObj;
};

function camelize(str) {
  let arr = str.split('-');
  let capital = arr.map((item, index) => index ? item.charAt(0).toUpperCase() + item.slice(1).toLowerCase() : item.toLowerCase());
  // ^-- change here.
  return capital.join("");
}

const cleanIndexSignatures = (schema, typescriptText) => {
// c return  typescriptText;
  const acum = []
  let count = 0;

  const typescriptTestList = typescriptText.split('{')

  if (typescriptTestList.length <= 1) {
    return typescriptText;
  }

  acum.push(`${typescriptTestList.shift()}{`)
  typescriptText = typescriptTestList.join('{')

  typescriptText = recursiveCleanIndexSignatures(schema, typescriptText, count, acum)
  acum.push(typescriptText)

  return acum.join('');
}

const breakIntoSections = (typescriptText) => {
  const openBracketSections = typescriptText.split('{')
  let openBracketCount = 1; let i = 0;
  while (openBracketCount > 0) {
    i++;
    openBracketCount = i;
    const strTarget = openBracketSections[i]

    let j;
    for (j=0; j < strTarget.length; j++) {
      if (strTarget[j] === '}') {
        openBracketCount--;
      }

      if (openBracketCount === 0) {
        break;
      }
    }
  }

  const typescriptTextTarget = openBracketSections.slice(0, i + 1).join("{")
  const typescriptTextAfter = openBracketSections.slice(i + 1).join("{")

  return [typescriptTextTarget, typescriptTextAfter]
}

const recursiveCleanIndexSignatures = (schema, typescriptText, count, acum) => {
  if (schema.additionalProperties === false) {
    const str = '\n' + '  '.repeat(count + 1) + '[k: string]: unknown;';
    typescriptText = typescriptText.replace(str, '');
  }

  if (schema.properties) {
    Object.keys(schema.properties).forEach((key) => {
      if (schema.properties[key].type === 'array' && schema.properties[key].items.type !== 'string') {
        const typescriptTestList = typescriptText.split(key);
        acum.push(`${typescriptTestList.shift()}${key}`);
        typescriptText = typescriptTestList.join(key);

        let [typescriptTextTarget, typescriptTextAfter] = breakIntoSections(typescriptText);

        typescriptTextTarget = recursiveCleanIndexSignatures(schema.properties[key].items, typescriptTextTarget, count + 1, acum);
        typescriptText = [typescriptTextTarget, typescriptTextAfter].filter((item) => item !== '').join("{");
      }

      if (schema.properties[key].type === 'object') {
        const typescriptTestList = typescriptText.split(key)
        acum.push(`${typescriptTestList.shift()}${key}`)
        typescriptText = typescriptTestList.join(key)

        let [typescriptTextTarget, typescriptTextAfter] = breakIntoSections(typescriptText);

        typescriptTextTarget = recursiveCleanIndexSignatures(schema.properties[key], typescriptTextTarget, count + 1, acum)
        typescriptText = [typescriptTextTarget, typescriptTextAfter].filter((item) => item !== '').join('{');
      }
    });
  }

  return typescriptText;
}

const buildTypes = async (spec, destinationFolder) => {
  const responseTypes = [];
  const pathTypes = [];
  const bodyTypes = [];
  const queryTypes = [];
  const dataTypes = [];

  Object.keys(spec.paths).forEach((path) => {
    Object.keys(spec.paths[path]).forEach((verb) => {
      const { responses, operationId, parameters, requestBody } = spec.paths[path][verb];

      const httpSuccessKey = Object.keys(responses).filter((item) => item[0] === '2');
      const jsonSchemaBody = _.get(requestBody, 'content.application/json.schema') || _.get(requestBody, 'content.multipart/form-data.schema');

      let queryParams = [];
      let pathParams = [];
      if (parameters) {
        queryParams = parameters.filter((item) => item.in === 'query');
        pathParams = parameters.filter((item) => item.in === 'path');
      }

      if (operationId) {
        bodyTypes.push(compile(
          jsonSchemaBody || {
            type: 'object',
            properties: {},
            required: [],
          },
          `${operationId}B`,
        ).then((res) => {
          if (jsonSchemaBody) {
            return cleanIndexSignatures(jsonSchemaBody, res)
          }
          return res;
        }));

        const jsonSchemaQuery = {
          type: 'object',
          properties: {},
          required: [],
          additionalProperties: false,
        };

        queryParams.forEach((item) => {
          jsonSchemaQuery.properties[item.name] = {
            type: item.schema.type,
            description: item.description,
            ...(item.schema.type === 'array' && {
              items: {
                type: item.schema.items.type,
              },
            }),
          };
          if (item.required) jsonSchemaQuery.required.push(item.name);
        });

        queryTypes.push(compile(
          jsonSchemaQuery,
          `${operationId}Q`,
        ).then((res) =>  {
          if (jsonSchemaQuery) {
            return cleanIndexSignatures(jsonSchemaQuery, res)
          }
        }));

        const jsonSchemaPath = {
          type: 'object',
          properties: {},
          required: [],
          additionalProperties: false,
        };

        pathParams.forEach((item) => {
          jsonSchemaPath.properties[item.name] = {
            type: item.schema.type,
            description: item.description,
          };
          if (item.required) jsonSchemaPath.required.push(item.name);
        });

        pathTypes.push(compile(
          jsonSchemaPath,
          `${operationId}P`,
        ).then((res) => {
          if (jsonSchemaPath) {
            return cleanIndexSignatures(jsonSchemaPath, res)
          }
        }));

        const responseSchema = _.get(responses[httpSuccessKey], 'content.application/json.schema');
        if (responseSchema) {
          responseTypes.push(compile(responseSchema, `${operationId}R`).then((res) => {
            if (responseSchema) {
              return cleanIndexSignatures(responseSchema, res)
            }
            return res;
          }));
        }
      }
    });
  });

  Object.keys(spec.components.schemas).forEach(dataObjKey => {
    dataTypes.push(compile(
      spec.components.schemas[dataObjKey],
      `${dataObjKey}`,
    ).then((res) => {
      if (spec.components.schemas[dataObjKey]) {
        return cleanIndexSignatures(spec.components.schemas[dataObjKey], res)
      }
    }));
  });

  fs.writeFileSync(
    `${destinationFolder}/responseTypes.ts`,
    (await Promise.all(responseTypes)).reduce((acc, item) => {
      return acc + item;
    }, ''),
  );

  fs.writeFileSync(
    `${destinationFolder}/bodyTypes.ts`,
    (await Promise.all(bodyTypes)).reduce((acc, item) => {
      return acc + item;
    }, ''),
  );

  fs.writeFileSync(
    `${destinationFolder}/queryTypes.ts`,
    (await Promise.all(queryTypes)).reduce((acc, item) => {
      return acc + item;
    }, ''),
  );

  fs.writeFileSync(
    `${destinationFolder}/pathTypes.ts`,
    (await Promise.all(pathTypes)).reduce((acc, item) => {
      return acc + item;
    }, ''),
  );

  fs.writeFileSync(
    `${destinationFolder}/dataTypes.ts`,
    (await Promise.all(dataTypes)).reduce((acc, item) => {
      return acc + item;
    }, ''),
  );

}

module.exports = {
  replaceRefs,
  cleanIndexSignatures,
  camelize,
  buildTypes,
};
